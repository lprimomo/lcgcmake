#---List of externals----------------------------------------------i
set(LCG_PYTHON_VERSION 3)
include(heptools-dev4)

LCG_external_package(ROOT 6.24.06 CUDA=OFF)

# temporary version settings for LCG_101, review for later chains
LCG_external_package(fjcontrib         1.046                        )
LCG_external_package(valgrind          3.18.1                       )


LCG_external_package(evtgen            1.7.0          ${MCGENPATH}/evtgen     tag=R01-07-00 hepmc=2)
LCG_external_package(thepeg            2.2.1          ${MCGENPATH}/thepeg     hepmc=2)
LCG_external_package(herwig3           7.2.1          ${MCGENPATH}/herwig++   thepeg=2.2.1 madgraph=3.1.2.beta.atlas1 openloops=2.1.1 lhapdf=6.2.3 hepmc=2)

LCG_external_package(sherpa            2.2.11p2          ${MCGENPATH}/sherpa  hepmc=both hepevt=200000 author=2.2.11)
LCG_external_package(sherpa-openmpi    2.2.11p2.openmpi3 ${MCGENPATH}/sherpa  hepmc=both hepevt=200000 author=2.2.11)

LCG_external_package(photos++          3.61     ${MCGENPATH}/photos++ author=3.61   )
LCG_external_package(tauola++          1.1.6    ${MCGENPATH}/tauola++ author=1.1.6  )

LCG_external_package(pythia8           306            ${MCGENPATH}/pythia8    zlib=ON     )
LCG_external_package(yoda              1.9.0          ${MCGENPATH}/yoda                   )
LCG_external_package(rivet             3.1.5p1          ${MCGENPATH}/rivet      hepmc=2  author=3.1.5  )
LCG_external_package(lhapdf            6.2.3          ${MCGENPATH}/lhapdf )

LCG_external_package(openloops         2.1.2          ${MCGENPATH}/openloops )
LCG_external_package(superchic         4.02.2         ${MCGENPATH}/superchic    author=4.02 )
LCG_external_package(madgraph5amc      3.3.1.atlas1   ${MCGENPATH}/madgraph5amc author=3.3.1)
LCG_external_package(apfel             3.0.4          ${MCGENPATH}/apfel )

LCG_remove_package(R)
LCG_remove_package(rpy2)
LCG_remove_package(DD4hep)
LCG_remove_package(acts)

#--- CUDA et al. --------------------------------------------------
LCG_external_package(cuda         11.4     full=11.4.2_470.57.02)
LCG_external_package(cudnn        8.2.4.15 )


LCG_top_packages(AIDA Boost CppUnit Davix Frontier_Client GSL HepMC HepPDT PyYAML Python Qt5 RELAX ROOT XercesC apfel blas chardet clhep coin3d cppgsl crmc cryptography cx_oracle cycler dcap decorator distro doxygen entrypoints eigen evtgen expat fastjet fjcontrib fmt freetype future gperftools graphviz gtest heaptrack hdf5 hepmcanalysis heputils herwig3 hijing hydjet idna importlib_metadata ipython java jsonmcpp kiwisolver lhapdf libffi libsodium libunwind libxkbcommon libxml2 libxslt lxml lz4 madgraph5amc matplotlib maven mccabe mctester mcutils mysql oracle pandas pcre pip png pprof protobuf psutil pycodestyle pyflakes pygraphviz pyqt5 pyquen pythia6 pythia8 rangev3 requests rivet scipy sherpa sherpa-openmpi six sqlalchemy sqlite starlight stomppy superchic tbb thepeg tiff urllib3 valgrind vbfnlo vdt xrootd yoda zeromq zipp zlib libaio ccache igprof cuda cudnn coverage)
