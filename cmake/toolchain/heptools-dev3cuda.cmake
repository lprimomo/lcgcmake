#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT         HEAD   GIT=http://root.cern.ch/git/root.git        )
LCG_external_package(hepmc3       HEAD   GIT=https://gitlab.cern.ch/hepmc/HepMC3.git )

#---Remove unneeded packages---------------------------------------
LCG_remove_package(Geant4)
LCG_remove_package(DD4hep)
LCG_remove_package(acts)
LCG_remove_package(Gaudi)
LCG_remove_package(Garfield++)
# Require python 3.9
LCG_remove_package(DecayLanguage)
LCG_remove_package(particle)

#---Overwrites and additional packages ----------------------------
# llvm library is needed for apache-tvm 
LCG_external_package(llvmmin           14.0.3                                   )
LCG_external_package(tvm               0.8.0                                    )
LCG_external_package(cuda         11.8     full=11.8.0_520.61.05     )
LCG_external_package(cudnn        8.6.0.163 cuda=11               )
LCG_external_package(TensorRT     8.5.3.1  cuda=11.8 cudnn=8.6     )

LCG_external_package(pycuda       2021.1                        )
LCG_external_package(appdirs      1.4.4                         )
LCG_external_package(py_tools     2021.2.7                      )
LCG_external_package(pybind11     2.6.2                         )
LCG_external_package(pyopencl     2021.2.2                      )
LCG_external_package(mako         1.1.4                         )
LCG_external_package(cupy         11.1.0    cuda=11x            )
LCG_external_package(fastrlock    0.6                           )

#----Overwites for Ubuntu20---------------------------------------
if( ${LCG_OS}${LCG_OSVERS} MATCHES ubuntu20 )
  LCG_remove_package(TensorRT)
endif()
