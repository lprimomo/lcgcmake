#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev3)

SET(LHCB_JSON_FILE https://gitlab.cern.ch/lhcb-core/rpm-recipes/-/raw/master/LHCBEXTERNALS/dev3lhcb.json)

include(heptools-lhcbsetup)