#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev4)

SET(LHCB_JSON_FILE https://gitlab.cern.ch/lhcb-core/rpm-recipes/-/raw/master/LHCBEXTERNALS/dev4lhcb.json)

include(heptools-lhcbsetup)