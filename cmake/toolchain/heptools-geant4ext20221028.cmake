#---List of externals--------------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Fix the versions for the most relevant packages -------------------
LCG_external_package(CMake             3.24.2                         )
LCG_external_package(ROOT              6.26.08                        )
LCG_external_package(veccore           0.8.0                          )
LCG_external_package(VecGeom           1.2.0                          )
LCG_external_package(clhep             2.4.6.0                        )
LCG_external_package(pybind11          2.10.0                         )
LCG_external_package(hdf5              1.12.2                         )

#---Define the top level packages for this stack-----------------------
LCG_top_packages(ROOT VecGeom Python Qt5 tbb XercesC clhep expat ninja
                 ccache CMake lcgenv hdf5 valgrind pybind11)
