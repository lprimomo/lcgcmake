#!/usr/bin/env bash

# create_rpms.sh <platform>[:compiler] <link> <rpm_repo> [--dbupdate|--nodbupdate] [<workdir>]
platform=$1
link=$2
rpm_repo=$(readlink -f $3)

dbupdate=""
if test ! "x$4" = "x" ; then
  if test "x$4" = "x--nodbupdate" ; then
    dbupdate="no"
  elif test "x$4" = "x--dbupdate" ; then
    dbupdate="yes"
  else
    workdir=$4
  fi
fi
if test ! "x$5" = "x" ; then
   workdir=$5
fi

srcdir=$(dirname $(readlink -f $0))

if [[ -n "$workdir" ]]; then
    cd $workdir
fi

resetdirs="yes"
# Is there a compiler specification?
compiler=`echo $platform | awk '{split($0,a,":"); print a[2]}'`
echo "compiler: $compiler"
if test ! "x$compiler" = "x" ; then
   platform=`echo $platform | awk '{split($0,a,":"); print a[1]}'`
   resetdirs="no"
fi

if test "x$resetdirs" = "no" ; then
   for d in "BUILD BUILDROOT RPMS SOURCES SPECS SRPMS" ; do
      dd="rpmbuild/$d"
      if test ! -d $dd ; then
         resetdirs="yes"
      fi
   done
fi

echo "resetdirs: $resetdirs"
 
# Requirements to run this script
# - rpmbuild, createrepo, xrootd
# - access to EOS
# - access to CVMFS sft.cern.ch

# Check requirements

# TODO

# This script requires the platform and the link to the tarball release area
# It is expected to find LCG_contrib_<platform>.txt and corresponding tarballs that are mentioned in the LCG_contrib_<platform>.txt file.

if [[ $PWD = /afs/* ]]; then
    echo "Detected that the current working directory is in afs."
    echo "This might lead to some problems while creating rpms."
    echo "Please change the current work directory by specifing <workdir> parameter"
    echo ""
    echo "The problem might occur when in the given package a hardlink is used."
    echo "Because afs does not support hardlinks, the created rpms might not be complete or other unforeseen problem might occur"
    exit 1
fi


# Assure that the directories are empty (if doing all)
if test "x$resetdirs" = "yes" ; then
   rm -rf rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}/* rpmbuild/LCG_contrib_*

   # Create rpmbuild structure
   mkdir -v -p rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}
fi

# Cleanup SOURCES in all cases
rm -rf rpmbuild/{SOURCES,SPECS}/*

# Download LCG_contrib_<platform>.txt
cd rpmbuild
if ! wget ${link}/LCG_contrib_${platform}.txt; then
    echo "Couldn't download LCG_contrib_${platform}.txt"
    echo "Don't know what to do; exit"
    exit 1
fi

# Download tarballs

for file in `python $srcdir/python/lcg/buildinfo.py --pattern "${link}/{NAME}-{VERSION}_{HASH}-${platform}.tgz" LCG_contrib_${platform}.txt`
do
    echo "file: $file"
    if [[ $file == *${compiler}* ]]; then
       echo "file: $file  -----> retrieving"
       fn=`basename $file`
       if test -f "SOURCES/$fn" ; then
          rm -fr SOURCES/$fn
       fi
       wget -P SOURCES/ $file
    fi
done

python $srcdir/create_LCG_contrib_SPEC.py --verbose --platform $platform SOURCES/*


# Generate rpms
export PATH=$srcdir/../source_env:$PATH # Add access to create_setup.py
for spec in SPECS/*; do
  echo Generate RPM from $spec

  rpmbuild -bb --buildroot=$PWD/BUILDROOT --define "_topdir `pwd`" \
    -D '__strip /cvmfs/sft.cern.ch/lcg/contrib/binutils/latest/x86_64-centos7/bin/strip' \
    -D '__objdump /cvmfs/sft.cern.ch/lcg/contrib/binutils/latest/x86_64-centos7/bin/objdump' \
    $spec
  # rpmbuild needs to point to a newer binutils then on the system.
  # In particular to strip and objdump, because it is checking the binaries and might complain if to old binutils
  # TODO: Don't point to a fixed version of binutils, point to the latest
done

if [[ $rpm_repo ]]; then

# Copy rpms to EOS
pushd RPMS/noarch
for rpmpkg in ./*.rpm; do
    if [[ $rpm_repo = /eos/* ]]; then
        xrdcp -f $rpmpkg root://eosuser.cern.ch/${rpm_repo}/$rpmpkg
    else
        cp $rpmpkg ${rpm_repo}/$rpmpkg
    fi
done
popd

# Update database
if test "x$dbupdate" = "xyes" ; then
   createrepo --update $rpm_repo
else
   echo "RPM db will not be updated"
fi

fi
