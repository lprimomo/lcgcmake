From d97dae7f5de3402f2555c4316717fbcd7531c5a7 Mon Sep 17 00:00:00 2001
From: Frank Siegert <frank.siegert@cern.ch>
Date: Tue, 2 Jun 2020 23:00:04 +0200
Subject: [PATCH] Bugfix for crash with `SPECIAL_TAU_SPIN_CORRELATIONS`

Events with partonic hadron decays involving a tau (e.g. B -> cb d tau vtau) and multiple parton interactions could trigger a core dump in the `SPECIAL_TAU_SPIN_CORRELATIONS=1` treatment. This happened due to an infinite loop, which is fixed now.
In addition, in events triggering a `Retry_Event` a minor problem with lost spin correlations is fixed.
---
 METOOLS/SpinCorrelations/Amplitude2_Matrix.C |  2 +-
 METOOLS/SpinCorrelations/Spin_Density.C      |  5 +++--
 SHERPA/Single_Events/Decay_Handler_Base.C    | 15 ++++++++-------
 3 files changed, 12 insertions(+), 10 deletions(-)

diff --git a/METOOLS/SpinCorrelations/Amplitude2_Matrix.C b/METOOLS/SpinCorrelations/Amplitude2_Matrix.C
index b8071acc7..2d1d3dc70 100644
--- METOOLS/SpinCorrelations/Amplitude2_Matrix.C
+++ METOOLS/SpinCorrelations/Amplitude2_Matrix.C
@@ -55,7 +55,7 @@ Complex Amplitude2_Matrix::operator*(const Amplitude2_Matrix& sigma) const
 namespace METOOLS {
   std::ostream& operator<<(std::ostream& ostr, const Amplitude2_Matrix& m) {
     ostr<<"   Matrix with "<<m.m_nhel<<" spin combinations for "
-        <<m.Particle()->RefFlav()<<":"<<std::endl;
+        <<(m.Particle()?m.Particle()->RefFlav():Flavour(kf_none))<<":"<<std::endl;
     for(size_t i=0;i<m.m_nhel;i++) {
       for(size_t j=0;j<m.m_nhel;j++) {
         ostr<<m(i,j)<<", ";
diff --git a/METOOLS/SpinCorrelations/Spin_Density.C b/METOOLS/SpinCorrelations/Spin_Density.C
index dbb7f4f7b..ae1de5dba 100644
--- METOOLS/SpinCorrelations/Spin_Density.C
+++ METOOLS/SpinCorrelations/Spin_Density.C
@@ -42,7 +42,7 @@ Spin_Density::~Spin_Density()
 {
 }
 
-typedef std::map<Particle*, Spin_Density*> SpinDensityMap;
+typedef std::vector<std::pair<std::pair<ATOOLS::Flavour,ATOOLS::Vec4D>, Spin_Density*> > SpinDensityMap;
 namespace ATOOLS {
   template <> Blob_Data<SpinDensityMap*>::~Blob_Data()
   {
@@ -56,7 +56,8 @@ namespace ATOOLS {
   {
     SpinDensityMap* newdata = new SpinDensityMap();
     for (SpinDensityMap::iterator it = m_data->begin(); it!=m_data->end(); ++it) {
-      newdata->insert(make_pair(it->first, new Spin_Density(*it->second)));
+      std::pair<ATOOLS::Flavour,ATOOLS::Vec4D> first = std::make_pair(it->first.first, it->first.second);
+      newdata->push_back(make_pair(first, new Spin_Density(*it->second)));
     }
     return new Blob_Data(newdata);
   }
diff --git a/SHERPA/Single_Events/Decay_Handler_Base.C b/SHERPA/Single_Events/Decay_Handler_Base.C
index 8106a68ef..f1aa89104 100644
--- SHERPA/Single_Events/Decay_Handler_Base.C
+++ SHERPA/Single_Events/Decay_Handler_Base.C
@@ -183,7 +183,7 @@ Blob* FindSPBlob(Blob* startblob)
 }
 
 
-typedef map<Particle*, Spin_Density*> SpinDensityMap;
+typedef std::vector<std::pair<std::pair<ATOOLS::Flavour,ATOOLS::Vec4D>, Spin_Density*> > SpinDensityMap;
 bool Decay_Handler_Base::DoSpecialDecayTauSC(Particle* part)
 {
   DEBUG_FUNC(*part);
@@ -207,9 +207,8 @@ bool Decay_Handler_Base::DoSpecialDecayTauSC(Particle* part)
 
   double bestDeltaR=1000.0; Spin_Density* sigma_tau=NULL;
   for (SpinDensityMap::iterator it=tau_spindensity->begin(); it!=tau_spindensity->end(); ++it) {
-    Particle* testpart = it->first;
-    if (testpart->Flav()==part->Flav()) {
-      double newDeltaR=part->Momentum().DR(testpart->Momentum());
+    if (it->first.first==part->Flav()) {
+      double newDeltaR=part->Momentum().DR(it->first.second);
       if (newDeltaR<bestDeltaR) {
         bestDeltaR=newDeltaR;
         sigma_tau=it->second;
@@ -378,7 +377,7 @@ Decay_Matrix* Decay_Handler_Base::FillDecayTree(Blob * blob, Spin_Density* s0)
          blob->Has(blob_status::needs_showers))) {
       DEBUG_INFO("is stable.");
       if (m_specialtauspincorr && daughters[i]->Flav().Kfcode()==kf_tau &&
-          !daughters[i]->Flav().IsStable() &&
+          !daughters[i]->Flav().IsStable() && blob->Type()==btp::Hard_Decay &&
           rpa->gen.SoftSC()) {
         DEBUG_INFO("  keeping tau spin information for hadronic tau decays.");
         SpinDensityMap* tau_spindensity;
@@ -392,8 +391,10 @@ Decay_Matrix* Decay_Handler_Base::FillDecayTree(Blob * blob, Spin_Density* s0)
         else {
           tau_spindensity = bdb->Get<SpinDensityMap*>();
         }
-        (*tau_spindensity)[daughters[i]] = new Spin_Density(daughters[i],amps);
-        DEBUG_VAR(*(*tau_spindensity)[daughters[i]]);
+        tau_spindensity->push_back(make_pair(make_pair(daughters[i]->Flav(), daughters[i]->Momentum()),
+                                             new Spin_Density(daughters[i],amps)));
+        DEBUG_VAR(*(tau_spindensity->back().second));
+        tau_spindensity->back().second->SetParticle(NULL);
       }
       else if (m_spincorr) {
         Decay_Matrix* D=new Decay_Matrix(daughters[i]);
diff --git a/METOOLS/SpinCorrelations/Spin_Density.C b/METOOLS/SpinCorrelations/Spin_Density.C
index ae1de5dba0a603de368579f45c5582f9b6f3bee9..71f38938c10cc40c9c0e5e9421dda43a76768810 100644
--- METOOLS/SpinCorrelations/Spin_Density.C
+++ METOOLS/SpinCorrelations/Spin_Density.C
@@ -42,7 +42,6 @@ Spin_Density::~Spin_Density()
 {
 }
 
-typedef std::vector<std::pair<std::pair<ATOOLS::Flavour,ATOOLS::Vec4D>, Spin_Density*> > SpinDensityMap;
 namespace ATOOLS {
   template <> Blob_Data<SpinDensityMap*>::~Blob_Data()
   {
diff --git a/METOOLS/SpinCorrelations/Spin_Density.H b/METOOLS/SpinCorrelations/Spin_Density.H
index c4ff5fbf94b819ef021cd6687e37d4a9a05cb850..a23b07481d769652c84e271c18a10896cdd668ca 100644
--- METOOLS/SpinCorrelations/Spin_Density.H
+++ METOOLS/SpinCorrelations/Spin_Density.H
@@ -3,6 +3,9 @@
 
 #include <vector>
 #include "ATOOLS/Math/MyComplex.H"
+#include "ATOOLS/Phys/Flavour.H"
+#include "ATOOLS/Phys/Blob.H"
+#include "ATOOLS/Math/Vector.H"
 #include "METOOLS/SpinCorrelations/Amplitude2_Matrix.H"
 
 namespace ATOOLS {
@@ -26,5 +29,11 @@ namespace METOOLS {
 
 }
 
+typedef std::vector<std::pair<std::pair<ATOOLS::Flavour,ATOOLS::Vec4D>, METOOLS::Spin_Density*> > SpinDensityMap;
+namespace ATOOLS {
+  template <> Blob_Data<SpinDensityMap*>::~Blob_Data();
+
+  template <> Blob_Data_Base* Blob_Data<SpinDensityMap*>::ClonePtr();
+}
 
 #endif
diff --git a/PHASIC++/Main/Phase_Space_Handler.C b/PHASIC++/Main/Phase_Space_Handler.C
index a4fc706ed6aa7147865f77b7bf62b38245af9b28..343d1c97c821ca558e70244a20ca7054d6c3e85c 100644
--- PHASIC++/Main/Phase_Space_Handler.C
+++ PHASIC++/Main/Phase_Space_Handler.C
@@ -559,25 +559,8 @@ void Phase_Space_Handler::AddPoint(const double value)
     if (p_isrchannels)  p_isrchannels->AddPoint(value*enhance);
     p_fsrchannels->AddPoint(value*enhance);
     if (p_enhancehisto) {
-      if (!p_process->Process()->Info().Has(nlo_type::rsub)) {
-	double obs((*p_enhanceobs)(&p_lab.front(),
-                                   &p_flavours.front(),m_nin+m_nout));
-	p_enhancehisto_current->Insert(obs,value/m_enhance);
-      }
-      else {
-        // fixed-order RS, fill with RS weight and R kinematics
-        if (p_process->Process()->Info().m_nlomode==1) {
-          double obs((*p_enhanceobs)(&p_lab.front(),
-                                     &p_flavours.front(),m_nin+m_nout));
-          p_enhancehisto_current->Insert(obs,value/m_enhance);
-	}
-        // MC@NLO H/RS, fill with H weight and kinematics
-        else {
-          double obs((*p_enhanceobs)(&p_lab.front(),
-                                     &p_flavours.front(),m_nin+m_nout));
-          p_enhancehisto_current->Insert(obs,value/m_enhance);
-        }
-      }
+      double obs((*p_enhanceobs)(&p_lab.front(),&p_flavours.front(),m_nin+m_nout));
+      p_enhancehisto_current->Insert(obs,value/m_enhance);
     }
   }
 }
@@ -634,20 +617,7 @@ void Phase_Space_Handler::SetEnhanceFunction(const std::string &enhancefunc)
 double Phase_Space_Handler::EnhanceFactor(Process_Base *const proc)
 {
   if (p_enhanceobs==NULL) return 1.0;
-  double obs=p_enhancehisto?p_enhancehisto->Xmin():0.0;
-  if (!proc->Info().Has(nlo_type::rsub)) {
-    obs=(*p_enhanceobs)(&p_lab.front(),&p_flavours.front(),m_nin+m_nout);
-  }
-  else {
-    // fixed-order RS, read out with R kinematics
-    if (proc->Info().m_nlomode==1) {
-      obs=(*p_enhanceobs)(&p_lab.front(),&p_flavours.front(),m_nin+m_nout);
-    }
-    // MC@NLO H, read out with H kinematics
-    else {
-      obs=(*p_enhanceobs)(&p_lab.front(),&p_flavours.front(),m_nin+m_nout);
-    }
-  }
+  double obs=(*p_enhanceobs)(&p_lab.front(),&p_flavours.front(),m_nin+m_nout);
   if (p_enhancehisto==NULL) return obs;
   if (obs>=p_enhancehisto->Xmax()) obs=p_enhancehisto->Xmax()-1e-12;
   if (obs<=p_enhancehisto->Xmin()) obs=p_enhancehisto->Xmin()+1e-12;
diff --git a/PHASIC++/Main/Phase_Space_Handler.C b/PHASIC++/Main/Phase_Space_Handler.C
index 343d1c97c821ca558e70244a20ca7054d6c3e85c..3023c47de9ff5fc2c3360465785fdac9b9958d5c 100644
--- PHASIC++/Main/Phase_Space_Handler.C
+++ PHASIC++/Main/Phase_Space_Handler.C
@@ -40,6 +40,7 @@ Phase_Space_Handler::Phase_Space_Handler(Process_Integrator *proc,double error):
   m_name(proc->Process()->Name()), p_process(proc), p_active(proc), p_integrator(NULL), p_cuts(NULL),
   p_enhanceobs(NULL), p_enhancefunc(NULL), p_enhancehisto(NULL), p_enhancehisto_current(NULL),
   m_enhance(1.0),
+  m_enhancefunc_min(numeric_limits<double>::lowest()), m_enhancefunc_max(numeric_limits<double>::max()),
   p_variationweights(NULL),
   p_beamhandler(proc->Beam()), p_isrhandler(proc->ISR()), p_fsrchannels(NULL),
   p_isrchannels(NULL), p_beamchannels(NULL), p_massboost(NULL),
@@ -553,11 +554,10 @@ void Phase_Space_Handler::TestPoint(ATOOLS::Vec4D *const p,
 void Phase_Space_Handler::AddPoint(const double value)
 {
   p_process->AddPoint(value);
-  double enhance = EnhanceFunction();
   if (value!=0.0) {
-    if (p_beamchannels) p_beamchannels->AddPoint(value*enhance);
-    if (p_isrchannels)  p_isrchannels->AddPoint(value*enhance);
-    p_fsrchannels->AddPoint(value*enhance);
+    if (p_beamchannels) p_beamchannels->AddPoint(value);
+    if (p_isrchannels)  p_isrchannels->AddPoint(value);
+    p_fsrchannels->AddPoint(value);
     if (p_enhancehisto) {
       double obs((*p_enhanceobs)(&p_lab.front(),&p_flavours.front(),m_nin+m_nout));
       p_enhancehisto_current->Insert(obs,value/m_enhance);
@@ -576,7 +576,7 @@ void Phase_Space_Handler::SetEnhanceObservable(const std::string &enhanceobs)
     while(std::getline(ss, item, '|')) {
       parts.push_back(item);
     }
-    if (parts.size()<3 || parts.size()>4)
+    if (parts.size()<3)
       THROW(fatal_error,"Wrong syntax in enhance observable.");
     p_enhanceobs = Enhance_Observable_Base::Getter_Function::GetObject
       (parts[0],Enhance_Arguments(p_process->Process(),parts[0]));
@@ -604,36 +604,50 @@ void Phase_Space_Handler::SetEnhanceFunction(const std::string &enhancefunc)
 {
   if (enhancefunc!="1") {
     if (p_enhancefunc)
-      THROW(fatal_error,"Attempting to overwrite enhance function");
+      THROW(fatal_error, "Overwriting ME enhancefunc.");
+    vector<string> parts;
+    stringstream ss(enhancefunc);
+    string item;
+    while(std::getline(ss, item, '|')) {
+      parts.push_back(item);
+    }
+    if (parts.size()<1)
+      THROW(fatal_error,"Wrong syntax in enhancefunc.");
     p_enhancefunc = Enhance_Observable_Base::Getter_Function::GetObject
-      (enhancefunc,Enhance_Arguments(p_process->Process(),enhancefunc));
+      (parts[0],Enhance_Arguments(p_process->Process(),parts[0]));
     if (p_enhancefunc==NULL) {
       msg_Error()<<METHOD<<"(): Enhance function not found. Try 'VAR{..}'.\n";
-      THROW(fatal_error,"Invalid enhance function.");
+      THROW(fatal_error,"Invalid enhancefunc");
+    }
+    if (parts.size()>2) {
+      m_enhancefunc_min=ToType<double>(parts[1]);
+      m_enhancefunc_max=ToType<double>(parts[2]);
     }
   }
 }
 
 double Phase_Space_Handler::EnhanceFactor(Process_Base *const proc)
 {
-  if (p_enhanceobs==NULL) return 1.0;
-  double obs=(*p_enhanceobs)(&p_lab.front(),&p_flavours.front(),m_nin+m_nout);
-  if (p_enhancehisto==NULL) return obs;
-  if (obs>=p_enhancehisto->Xmax()) obs=p_enhancehisto->Xmax()-1e-12;
-  if (obs<=p_enhancehisto->Xmin()) obs=p_enhancehisto->Xmin()+1e-12;
-  double dsigma=p_enhancehisto->Bin(obs);
-  if (dsigma<=0.0) {
-    PRINT_INFO("Warning: Tried enhancement with dsigma/dobs("<<obs<<")="<<dsigma<<".");
-    dsigma=1.0;
-  }
-  if (m_enhancexs && p_process->TotalXS()>0.0) return 1.0/dsigma/p_process->TotalXS();
-  else return 1.0/dsigma;
-}
-
-double Phase_Space_Handler::EnhanceFunction()
-{
-  if (p_enhancefunc==NULL) return 1.0;
-  else return (*p_enhancefunc)(&p_lab.front(),&p_flavours.front(),m_nin+m_nout);
+  double enhanceweight=1.0;
+  if (p_enhanceobs) {
+    double obs=(*p_enhanceobs)(&p_lab.front(),&p_flavours.front(),m_nin+m_nout);
+    if (obs>=p_enhancehisto->Xmax()) obs=p_enhancehisto->Xmax()-1e-12;
+    if (obs<=p_enhancehisto->Xmin()) obs=p_enhancehisto->Xmin()+1e-12;
+    double dsigma=p_enhancehisto->Bin(obs);
+    if (dsigma<=0.0) {
+      PRINT_INFO("Warning: Tried enhancement with dsigma/dobs("<<obs<<")="<<dsigma<<".");
+      dsigma=1.0;
+    }
+    if (m_enhancexs && p_process->TotalXS()>0.0) enhanceweight *= 1.0/dsigma/p_process->TotalXS();
+    else enhanceweight *= 1.0/dsigma;
+  }
+  if (p_enhancefunc) {
+    double obs=(*p_enhancefunc)(&p_lab.front(),&p_flavours.front(),m_nin+m_nout);
+    obs = max(obs, m_enhancefunc_min);
+    obs = min(obs, m_enhancefunc_max);
+    enhanceweight *= obs;
+  }
+  return enhanceweight;
 }
 
 void Phase_Space_Handler::MPISync()
diff --git a/PHASIC++/Main/Phase_Space_Handler.H b/PHASIC++/Main/Phase_Space_Handler.H
index bc12361721e1274b26e2b70bc65f86a28e055e15..57f8ce9972d82a010435f980889ceba967a1b194 100644
--- PHASIC++/Main/Phase_Space_Handler.H
+++ PHASIC++/Main/Phase_Space_Handler.H
@@ -65,6 +65,7 @@ namespace PHASIC {
     Cut_Data               *p_cuts;  
 
     Enhance_Observable_Base *p_enhanceobs, *p_enhancefunc;
+    double m_enhancefunc_min, m_enhancefunc_max;
 
     ATOOLS::Histogram *p_enhancehisto, *p_enhancehisto_current;
 
@@ -136,8 +137,6 @@ namespace PHASIC {
 
     void AddPoint(const double xs);
 
-    double EnhanceFunction();
-
     void CalculateME();
     void CalculatePS();
     void CalculateEnhance();
