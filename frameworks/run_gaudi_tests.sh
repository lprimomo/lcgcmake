#!/bin/bash

# Run Gaudi tests from the build directory
# run-gaudi-tests.sh <local-prefix>  <platform>  <build-dir>

PREFIX=$1
PLATFORM=$2
BINARY_DIR=$3


# where is lcgenv?
if [ -z "$LCGENV" ]; then
  if command -v lcgenv &>/dev/null; then
     LCGENV=`command -v lcgenv`
  elif [ -x /cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv ]; then
     LCGENV=/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv
  fi
fi

# how many cores?
CPUS=`grep -c ^processor /proc/cpuinfo`   

# set current directory and environment
cd ${BINARY_DIR}
#eval "`$LCGENV -p ${PREFIX} ${PLATFORM} Gaudi`"
#env | sort | sed 's/:/:?     /g' | tr '?' '\n'

# set a DISPLAY to silence a warning in root
# see https://gitlab.cern.ch/gaudi/Gaudi/-/issues/243
export DISPLAY=:99

IGNORE="cmake.test_dummyGaudiDownstreamProject|GaudiConfiguration.nose|GaudiHive.experiments.atlas.mc_reco|GaudiExamples.event_timeout_abort|GaudiExamples.tupleex3"
ctest --version
ctest -j${CPUS} --output-on-failure --repeat until-pass:5 -E ${IGNORE}
